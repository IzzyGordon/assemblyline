import subprocess


def git_repo_revision(path):
    # Try the proper way
    p = subprocess.Popen(["git", "-C", path, "rev-parse", "HEAD"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, _ = p.communicate()
    if p.returncode == 0:
        lines = stdout.splitlines()
        if lines:
            return lines[0].strip()

    # Fall back to a more rough method
    p = subprocess.Popen("(cd {}; git rev-parse HEAD)".format(path), shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, _ = p.communicate()
    if p.returncode == 0:
        lines = stdout.splitlines()
        if lines:
            return lines[0].strip()

    raise ValueError("Path may not be git repo: " + path)
