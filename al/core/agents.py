#!/usr/bin/env python

from __future__ import absolute_import

import logging
import os
import pprint
import psutil
import shutil
import tempfile
import threading
import time
import signal
import subprocess
import uuid

from assemblyline.common import isotime
from assemblyline.common.exceptions import get_stacktrace_info
from assemblyline.common import net
from assemblyline.common import sysinfo
from assemblyline.al.common import forge
from assemblyline.al.common.message import Message, MT_SVCHEARTBEAT
from assemblyline.al.common.message import send_rpc, reply_to_rpc
from assemblyline.al.common.queue import CommsQueue, NamedQueue, LocalQueue, reply_queue_name
from assemblyline.al.common.task import Task
from assemblyline.al.core.servicing import ServiceManager
config = forge.get_config()


class RemoteShutdownInterrupt(Exception):
    pass


class UnsupportedRequestError(Exception):
    pass


class ProvisioningError(Exception):
    pass


DEFAULT_REGISTRATION = {
    'ip': '',
    'hostname': '',
    'mac_address': '',
    'enabled': True,
    'is_vm': False,
    'hosts_override': {},
    'vm_host': '',
    'source_rev': ''
}
VM_MAC_PREFIX = '5254'
DATABASE_NUM = 3


# noinspection PyBroadException
def worker_cleanup(mac, logger=None):
    """
    Cleanup is responsible for performing resource cleanup for workers in the event that they're killed
    :param mac: the mac address of the host to cleanup
    :param logger: optional logger
    """
    try:
        if logger:
            logger.info('Worker resource cleanup starting.')
        persistent_settings = {
            'db': config.core.redis.persistent.db,
            'host': config.core.redis.persistent.host,
            'port': config.core.redis.persistent.port,
        }
        queue_name = "cleanup-%s" % mac
        cleanupq = NamedQueue(queue_name, **persistent_settings)

        def exhaust():
            while True:
                res = cleanupq.pop(blocking=False)
                if res is None:
                    break
                yield res

        ops = [op for op in exhaust()]
        for op in ops:
            # Execute the cleanup operation
            if isinstance(op, dict):
                op_type = op.get('type')
                op_args = op.get('args')
                if op_type == 'shell' and op_args is not None:
                    if logger:
                        logger.info('Executing cleanup command: %s', str(op_args))
                    subprocess.Popen(args=op_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if logger:
            logger.info("Worker resource cleanup complete.")
    except Exception:
        if logger:
            logger.exception("Worker resource cleanup failed: ")


class InvalidRequest(Exception):
    pass


class AgentRequest(Message):
    """ Convenience for HostAgent specific Message's."""

    HEARTBEAT = 'heartbeat'

    PING = 'ping'
    DRAIN = 'drain'
    UNDRAIN = 'undrain'
    SHUTDOWN = 'shutdown'
    SOFT_RESET = 'soft_reset'

    VM_LIST = 'vm_list'
    VM_START = 'vm_start'
    VM_STOP = 'vm_stop'
    VM_STOP_ALL = 'vm_stop_all'
    VM_GET_REVERT_TIMES = 'vm_get_revert_times'
    VM_RESTART = 'vm_restart'
    VM_REFRESH_ALL = 'vm_refreshall'
    VM_REFRESH_FLEET = 'vm_refreshfleet'
    VM_REFRESH_INSTANCE = 'vm_refreshinstance'

    START_SERVICES = 'start_services'
    STOP_SERVICES = 'stop_services'

    START_X_SERVICE = "start_x_service"
    STOP_X_SERVICE = "stop_x_service"

    START_X_VM = "start_x_vm"
    STOP_X_VM = "stop_x_vm"

    # noinspection PyUnusedLocal
    def __init__(self, to, mtype, body, sender=None, reply_to=None):
        valid_requests = [val for key, val in AgentRequest.__dict__.iteritems()
                          if isinstance(val, basestring) and not str(key).startswith("__")]
        if mtype not in valid_requests:
            raise InvalidRequest("'%s' is not a valid AgentRequest" % mtype)

        super(AgentRequest, self).__init__(to, mtype, sender, reply_to=None, body=body)

    @classmethod
    def parse(cls, raw):
        return Message.parse(raw)

    @staticmethod
    def is_agent_request(msg):
        valid_requests = [val for key, val in AgentRequest.__dict__.iteritems()
                          if isinstance(val, basestring) and not str(key).startswith("__")]
        return msg.mtype in valid_requests


# noinspection PyTypeChecker
class HostAgent(object):

    def __init__(self):
        self.ip = net.get_hostip()
        self.mac = net.get_mac_for_ip(self.ip)
        self.store = forge.get_datastore()
        self.log = logging.getLogger('assemblyline.agent')
        self.log.info('Starting HostAgent: MAC[%s] STORE[%s]' % (self.mac, self.store))

        # This hosts registration from riak (Hosts tab in UI).
        self.registration = None
        self.service_manager = None
        self.vm_manager = None
        self.lock = None
        self.consumer_thread = None
        self._should_run = False
        self.host_profile = {}
        self.executor_thread = None

        # Chores are actions that we run periodically and which we coallesce
        # when the same chore is requested multiple times in the same tick.
        # Jobs are executed as they are received.
        self.jobs = LocalQueue()
        self.last_heartbeat = 0
        self.rpc_handlers = {
            AgentRequest.PING: self.ping,
            AgentRequest.DRAIN: self.drain,
            AgentRequest.UNDRAIN: self.undrain,
            AgentRequest.SHUTDOWN: self.shutdown,
            AgentRequest.VM_LIST: self.list_vms,
            AgentRequest.VM_START: self.start_vm,
            AgentRequest.VM_STOP: self.stop_vm,
            AgentRequest.VM_STOP_ALL: self.stop_all_vms,
            AgentRequest.VM_RESTART: self.restart_vm,
            AgentRequest.VM_REFRESH_ALL: self.refresh_vm_all,
            AgentRequest.VM_REFRESH_FLEET: self.refresh_vm_fleet,
            AgentRequest.VM_GET_REVERT_TIMES: self.vm_get_revert_times,
            AgentRequest.START_SERVICES: self.start_services,
            AgentRequest.STOP_SERVICES: self.stop_services,
            AgentRequest.START_X_SERVICE: self.start_x_service,
            AgentRequest.STOP_X_SERVICE: self.stop_x_service,
            AgentRequest.START_X_VM: self.start_x_vm,
            AgentRequest.STOP_X_VM: self.stop_x_vm,
            AgentRequest.HEARTBEAT: self.heartbeat,
            AgentRequest.SOFT_RESET: self.soft_reset
        }

        self._should_run = True

        # Fetch and update or host registration information in riak.
        # self._init_registration() defer registration until later

    def soft_reset(self, msg):
        # Turn off all components
        self.drain(msg)

        # Reset profiles
        self.host_profile = {'services': {}, 'system_overrides': {}, 'virtual_machines': {}}
        if self.vm_manager:
            self.vm_manager.cfg = {}

        # Restore all components
        self.undrain(msg)
        return True

    # noinspection PyUnresolvedReferences
    def register_host(self):
        if self.is_a_vm():
            return "This is a VM, no need to register."

        existing_reg = self.store.get_node(self.mac)
        if existing_reg:
            return "already registered: %s" % pprint.pformat(existing_reg)
        reg = DEFAULT_REGISTRATION.copy()
        reg['hostname'] = net.get_hostname()
        reg['ip'] = self.ip
        reg['mac_address'] = self.mac
        reg['machine_info'] = sysinfo.get_machine_info()
        reg['last_checkin'] = isotime.now_as_iso()
        reg['platform'] = sysinfo.get_platform()
        reg['created'] = time.asctime()
        if 'roles' not in reg:
            reg['roles'] = []
        if "controller" not in reg["roles"]:
            reg['roles'].append("controller")
        if "hostagent" not in reg["roles"]:
            reg['roles'].append("hostagent")
        self.store.save_node(self.mac, reg)
        return 'Registered %s with %s' % (self.mac, pprint.pformat(reg))

    def _init_queues(self):
        self.rpcqueue = NamedQueue(self.mac)
        self.rpcqueue.delete()

    def is_a_vm(self):
        if self.mac.startswith(VM_MAC_PREFIX) and config.workers.install_kvm:
            return True
        return False

    # noinspection PyUnresolvedReferences
    def _init_registration(self):
        if self.is_a_vm():
            nq = NamedQueue('vm-%s' % self.mac, db=DATABASE_NUM)
            reg = nq.pop()
            nq.push(reg)

            self.log.info('Updating our registration.')
            reg['hostname'] = net.get_hostname()
            reg['ip'] = self.ip
            reg['machine_info'] = sysinfo.get_machine_info()
            reg['last_checkin'] = isotime.now_as_iso()
            reg['platform'] = sysinfo.get_platform()
            reg['updated'] = time.asctime()
            reg['system_name'] = config.system.name
            if 'roles' not in reg:
                reg['roles'] = []
            if "hostagent" not in reg["roles"]:
                reg['roles'].append("hostagent")

        else:
            reg = self.store.get_node(self.mac)

            if not reg:
                self.log.info('This appears to be our first run on this host. Registering ourselves.')
                reg = DEFAULT_REGISTRATION.copy()
                reg['hostname'] = net.get_hostname()
                reg['ip'] = self.ip
                reg['mac_address'] = self.mac
                reg['machine_info'] = sysinfo.get_machine_info()
                reg['last_checkin'] = isotime.now_as_iso()
                reg['platform'] = sysinfo.get_platform()
                reg['created'] = time.asctime()
                if 'roles' not in reg:
                    reg['roles'] = []
                if "controller" not in reg["roles"]:
                    reg['roles'].append("controller")
                if "hostagent" not in reg["roles"]:
                    reg['roles'].append("hostagent")
                self.store.save_node(self.mac, reg)
            else:
                # Just do an update of the extra info in registration.
                self.log.info('Updating our registration.')
                reg['hostname'] = net.get_hostname()
                reg['ip'] = self.ip
                reg['machine_info'] = sysinfo.get_machine_info()
                reg['last_checkin'] = isotime.now_as_iso()
                reg['platform'] = sysinfo.get_platform()
                reg['updated'] = time.asctime()
                reg['system_name'] = config.system.name
                if 'roles' not in reg:
                    reg['roles'] = []
                if "controller" not in reg["roles"] and not reg.get('is_vm', False):
                    reg['roles'].append("controller")
                if "hostagent" not in reg["roles"]:
                    reg['roles'].append("hostagent")
                self.store.save_node(self.mac, reg)

        self.registration = reg

        msgs = forge.apply_overrides(reg.get('config_overrides', None))
        if msgs:
            self.log.info("Using %s.", " and ".join(msgs))

        self.log.info('Our registration: %s', pprint.pformat(self.registration))

    def _wait_for_networking(self, timeout):
        uid = uuid.uuid4().get_hex()
        for each_second in xrange(timeout):
            try:
                q = NamedQueue('hostagent-redischeck-%s' % uid)
                q.push('can i reach you')
                q.pop(timeout=1, blocking=False)
                return True
            except Exception as e:
                self.log.info('waiting for redis reachability. %s ', str(e))
        return False

    def _check_time_drift(self):
        dispatcher = '0'
        name = reply_queue_name('cli_get_time')
        t = Task({}, **{
            'state': 'get_system_time',
            'watch_queue': name,
        })
        forge.get_control_queue('control-queue-' + dispatcher).push(t.raw)
        nq = NamedQueue(name)
        r = nq.pop(timeout=5)
        if r is None or 'time' not in r:
            self.log.warn('timed out trying to determine dispatchers clock.')
            return

        clock_difference = abs(r['time'] - time.time())
        if clock_difference > 600:
            self.log.info('Dispatchers clock %s away from ours. Clocks are not set correctly',
                          clock_difference)
        else:
            self.log.debug('Clock drift from dispatcher: %s.', clock_difference)

    # noinspection PyBroadException
    def _clear_tempdir(self):
        # Clear our temporary folder of any files left from previous executions.
        try:
            altemp_dir = os.path.join(tempfile.gettempdir(), 'al')
            shutil.rmtree(altemp_dir, ignore_errors=True)
        except Exception:
            self.log.exception('while clearing temporary directory during sysprep')

    def sysprep(self):
        """Basic prep and return."""
        self._init_registration()
        self._init_queues()
        self.log.info('performing sysprep')
        self._clear_tempdir()
        self._wait_for_networking(20)
        self._check_time_drift()

        if not self.registration:
            raise ProvisioningError('Host registration not found.')

        if not self.registration.get('enabled', None):
            raise ProvisioningError('Host explicitly disabled.')

        self.host_profile = self.registration.get('profile_definition', {})

        self.log.info('Our profile: %s', pprint.pformat(self.host_profile))
        vm_config = self.host_profile.get('virtual_machines', {})
        # noinspection PyBroadException
        try:
            from assemblyline.al.common.vm import VmManager
            self.vm_manager = VmManager(vm_config)
            self.vm_manager.sysprep()
            self.log.info("VmManager is ready on this host: %s" % self.mac)
        except Exception:
            self.log.info("Could not prep VmManager on this host: %s" % self.mac)

        # if we are are running within a VM. patch hosts files.
        if self.is_a_vm():
            nq = NamedQueue('vm-%s' % self.mac, db=DATABASE_NUM)
            nq.push(self.registration)

    # noinspection PyUnusedLocal
    def undrain(self, msg):
        self.store = forge.get_datastore()
        if self.service_manager:
            self.service_manager.undrain()
        if self.vm_manager:
            self.vm_manager.undrain()
        return True

    # noinspection PyUnusedLocal
    def drain(self, msg):
        if self.service_manager:
            self.service_manager.drain()
        if self.vm_manager:
            self.vm_manager.drain()
        if self.store:
            self.store.close()
        return True

    # noinspection PyUnusedLocal
    def list_vms(self, _msg):
        return self.vm_manager.list_vms()

    # noinspection PyUnusedLocal
    def stop_all_vms(self, _msg):
        return self.vm_manager.stop_all()

    def start_vm(self, msg):
        instance_name = msg.body.get('name', None)
        return self.vm_manager.start_vm(instance_name)

    def start_x_vm(self, msg):
        if not self.vm_manager:
            # service manager not running
            return 'failure'

        self.vm_manager.start_x_vm(msg.body.get('name'), msg.body.get('count'))
        return 'started'

    def stop_vm(self, msg):
        instance_name = msg.body.get('name', None)
        return self.vm_manager.stop_vm(instance_name)

    def stop_x_vm(self, msg):
        if not self.vm_manager:
            # service manager not running
            return 'failure'

        self.vm_manager.stop_x_vm(msg.body.get('name'), msg.body.get('count'))
        return 'stopped'

    def restart_vm(self, msg):
        instance_name = msg.body.get('name', None)
        return self.vm_manager.restart_vm(instance_name)

    def refresh_vm_fleet(self, msg):
        fleet_name = msg.body.get('name', None)
        return self.vm_manager.refresh_fleet(fleet_name)

    # noinspection PyUnusedLocal
    def vm_get_revert_times(self, _msg):
        return self.vm_manager.get_revert_times()

    # noinspection PyUnusedLocal
    def refresh_vm_all(self, _msg):
        return self.vm_manager.refresh_all()

    @staticmethod
    def _handle_unknown_request(msg):
        raise Exception('Unknown message type: %s', msg.mtype)

    def start_services(self, _):
        self._start_services()
        return 'started'

    def start_x_service(self, msg):
        if not self.service_manager:
            # service manager not running
            return 'failure'

        self.service_manager.start_service(msg.body.get('name'), msg.body.get('count'))
        return 'started'

    # noinspection PyUnusedLocal
    def stop_services(self, msg):
        self._stop_services()
        return 'stopped'

    def stop_x_service(self, msg):
        if not self.service_manager:
            # service manager not running
            return 'failure'

        self.service_manager.stop_service(msg.body.get('name'), msg.body.get('count'))
        return 'stopped'

    @staticmethod
    def _handle_exception(msg, e):
        return 'Exception while processing msg %s: %s' % (msg.mtype, str(e))

    def _handle_request(self, msg):
        self.log.info('Processing RPC: %s', msg.mtype)
        handler = self.rpc_handlers.get(msg.mtype, self._handle_unknown_request)
        return handler(msg)

    # noinspection PyBroadException
    def _rpc_executor_thread_main(self):
        self.send_heartbeat()
        while self._should_run:
            try:
                self.log.debug('Checking for RPCs on %s. Waiting: %s',
                               self.rpcqueue.name, self.jobs.qsize())
                raw = self.rpcqueue.pop(timeout=1, blocking=True)
                if not raw:
                    continue

                # RPCs are in assemblyline.al.common.Message format.
                msg = None
                error = None
                try:
                    msg = AgentRequest.parse(raw)
                except Exception as e:
                    self.log.exception('While processing rpc: %s', raw)
                    error = str(e)

                # TODO should we just block instead of using job queue ?
                if msg:
                    self.jobs.push(msg)
                else:
                    reply_to_rpc(raw, response_body=error, succeeded=False)
            except KeyboardInterrupt:
                self._should_run = False
                self.log.error('Thread got CTL-C in consumer thread.')
                return
            except Exception:
                self.log.exception('Unhandled Exception in consumer thread.')
                time.sleep(2)
                continue

    def _complete_chores_if_due(self):
        now = time.time()
        since_last_heartbeat = now - self.last_heartbeat
        if abs(since_last_heartbeat) >= config.system.update_interval:
            self.send_heartbeat()
            self.last_heartbeat = now

    # noinspection PyUnusedLocal
    def ping(self, _msg):
        self.log.info('PING')
        return 'PONG'

    def heartbeat(self, _=None):
        heartbeat = {
            'mac': self.mac,
            'time': isotime.now_as_iso(),
            'registration': self.registration,
            'resources': {
                'cpu_usage.percent': psutil.cpu_percent(),
                'mem_usage.percent': psutil.virtual_memory().percent,
                'disk_usage.percent': psutil.disk_usage('/').percent,
                'disk_usage.free': psutil.disk_usage('/').free
            },
            'profile_definition': self.host_profile
        }

        vm_host_mac = self.registration.get('vm_host_mac', None)
        if vm_host_mac:
            heartbeat['vm_host_mac'] = vm_host_mac

        if self.vm_manager:
            heartbeat['vmm'] = self.vm_manager.get_stats()
        else:
            heartbeat['vmm'] = None

        if self.service_manager:
            heartbeat['services'] = {'status': 'up', 'details': self.service_manager.get_stats()}
        else:
            heartbeat['services'] = None

        return heartbeat

    def send_heartbeat(self):
        self.log.debug(r'heartbeat.')
        heartbeat = self.heartbeat()
        msg = Message(to='*', mtype=MT_SVCHEARTBEAT, sender=self.mac, body=heartbeat)
        CommsQueue('status').publish(msg.as_dict())

    @staticmethod
    def shutdown(msg):
        raise RemoteShutdownInterrupt(str(msg))

    # noinspection PyBroadException
    def start_components(self):
        if not self.registration:
            raise ProvisioningError('Host registration not found.')

        if not self.registration.get('enabled', None):
            raise ProvisioningError('Host explicitly disabled.')

        self.host_profile = self.registration.get('profile_definition', {})
        self.log.info('Our profile: %s', pprint.pformat(self.host_profile))
        # Prior to startup, remove any safe-start queues associated with our mac address.
        NamedQueue('safe-start-%s' % self.mac).delete()

        services_config = self.host_profile.get('services', {})
        config_overrides = self.registration.get('config_overrides', {})
        self.service_manager = ServiceManager(services_config, config_overrides)
        self.service_manager.start()

        vm_config = self.host_profile.get('virtual_machines', {})
        try:
            from assemblyline.al.common.vm import VmManager
            self.vm_manager = VmManager(vm_config)
            self.vm_manager.start()
            self.log.info("VM Manager is ready on mac: %s" % self.mac)
        except Exception:
            self.log.info("VM Manager is inactive on mac: %s" % self.mac)

    def _stop_services(self):
        if self.service_manager:
            self.log.info('Stopping ServiceManager')
            self.service_manager.shutdown()
            self.service_manager = None

    def _start_services(self):
        if self.service_manager:
            # already running
            return

        services_config = self.host_profile.get('services', None)
        if services_config:
            config_overrides = self.registration.get('config_overrides', {})
            self.service_manager = ServiceManager(services_config, config_overrides)
            self.service_manager.start()
        else:
            self.log.info('No services provisioned for this host.')

    def stop_components(self):
        if self.service_manager:
            self._stop_services()

        if self.vm_manager:
            self.log.info('Stopping VmManager.')
            self.vm_manager.shutdown()
            self.vm_manager = None

    def run(self):
        # Clean up any leftover resources from workers
        worker_cleanup(self.mac, self.log)
        # Start up the core components (service and vmm managers)
        # and then kick of the rpc receiver.
        self._init_registration()
        self._init_queues()
        self.start_components()
        self.executor_thread = threading.Thread(target=self._rpc_executor_thread_main, name='agent_rpc_consumer')
        self.executor_thread.start()

        while self._should_run:
            self._complete_chores_if_due()
            job = self.jobs.pop(timeout=0.5)
            if not job:
                continue

            succeeded = True
            try:
                result = self._handle_request(job)
            except RemoteShutdownInterrupt:
                reply_to_rpc(job, response_body='Host Agent Shutting down.', succeeded=True)
                raise
            except Exception as e:  # pylint:disable=W0703
                succeeded = False
                result = 'Error while completing job: %s' % str(e)
                self.log.exception('while completing job')

            reply_to_rpc(job, response_body=result, succeeded=succeeded)

        self.log.info('_should_run is false. exiting.')
        return

    def stop(self):
        self.log.info('Stopping: MAC[%s] STORE[%s]' % (self.mac, self.store))
        self._should_run = False
        self.stop_components()
        if self.consumer_thread:
            self.consumer_thread.join(5)

        if self.store:
            self.store.close()

        worker_cleanup(self.mac, self.log)

    # noinspection PyUnusedLocal
    def _stop_signal_handler(self, signal_num, interrupted_frame):
        self.log.info("Shutting down due to signal.")
        self.stop()

    def serve_forever(self):
        try:
            # Listen for our shutdown signal
            signal.signal(signal.SIGINT, self._stop_signal_handler)
            # Inject a message onto the agents queue.
            self.run()
        except KeyboardInterrupt:
            self.log.info('Shutting down due to KeyboardInterrupt.')
            self.stop()
        except RemoteShutdownInterrupt as ri:
            msg = 'Shutting down due to remote command: %s' % ri
            self.log.info(msg)
            self.stop()
        except Exception as ex:
            msg = 'Shutting down due to unhandled exception: %s' % get_stacktrace_info(ex)
            self.log.error(msg)
            self.stop()


class AgentClient(object):

    def __init__(self, async=False, sender=None, timeout=10):
        """ If sender is not specified the local MAC is used """
        self.sender = sender or net.get_mac_for_ip(net.get_hostip())
        self.async = async
        self.timeout = timeout

    def _send_agent_rpc(self, mac, command, args=None, async=None, timeout=None):
        result = send_rpc(AgentRequest(
            to=mac, mtype=command, body=args,
            sender=self.sender), async=async or self.async, response_timeout=timeout or self.timeout)

        if not self.async:
            if result:
                return result.body
            return 'timeout'
        else:
            return result


class VmmAgentClient(AgentClient):

    def __init__(self, async=False, sender=None):
        super(VmmAgentClient, self).__init__(async, sender)

    def list_vms(self, mac):
        return self._send_agent_rpc(mac, AgentRequest.VM_LIST)

    def get_revert_times(self, mac):
        return self._send_agent_rpc(mac, AgentRequest.VM_GET_REVERT_TIMES)

    def stop_all_vms(self, mac):
        return self._send_agent_rpc(mac, AgentRequest.VM_STOP_ALL)

    def start_vm(self, mac, vmname):
        return self._send_agent_rpc(mac, AgentRequest.VM_START, args={'name': vmname})

    def refresh_all(self, mac):
        return self._send_agent_rpc(mac, AgentRequest.VM_REFRESH_ALL)

    def refresh_fleet(self, mac, fleet):
        return self._send_agent_rpc(mac, AgentRequest.VM_REFRESH_FLEET, args={'name': fleet})

    def stop_vm(self, mac, vmname):
        return self._send_agent_rpc(mac, AgentRequest.VM_STOP, args={'name': vmname})

    def restart_vm(self, mac, vmname):
        return self._send_agent_rpc(mac, AgentRequest.VM_RESTART, args={'name': vmname})


class ServiceAgentClient(AgentClient):

    def __init__(self, sender=None, async=False, timeout=10):
        super(ServiceAgentClient, self).__init__(async, sender, timeout)

    def heartbeat(self, mac, async=None, timeout=None):
        return self._send_agent_rpc(mac, AgentRequest.HEARTBEAT, async=async, timeout=timeout)

    def shutdown(self, mac, async=None, timeout=None):
        return self._send_agent_rpc(mac, AgentRequest.SHUTDOWN, async=async, timeout=timeout)

    def drain(self, mac, async=None, timeout=None):
        return self._send_agent_rpc(mac, AgentRequest.DRAIN, async=async, timeout=timeout)

    def undrain(self, mac, async=None, timeout=None):
        return self._send_agent_rpc(mac, AgentRequest.UNDRAIN, async=async, timeout=timeout)

    def ping(self, mac, async=None, timeout=None):
        return self._send_agent_rpc(mac, AgentRequest.PING, async=async, timeout=timeout)

    def startservices(self, mac, async=None, timeout=None):
        return self._send_agent_rpc(mac, AgentRequest.START_SERVICES, async=async, timeout=timeout)

    def stopservices(self, mac, async=None, timeout=None):
        return self._send_agent_rpc(mac, AgentRequest.STOP_SERVICES, async=async, timeout=timeout)

    def start_x_service(self, mac, name, count, async=None, timeout=None):
        return self._send_agent_rpc(mac, AgentRequest.START_X_SERVICE, args={'name': name, 'count': count},
                                    async=async, timeout=timeout)

    def stop_x_service(self, mac, name, count, async=None, timeout=None):
        return self._send_agent_rpc(mac, AgentRequest.STOP_X_SERVICE, args={'name': name, 'count': count},
                                    async=async, timeout=timeout)

    def start_x_vm(self, mac, name, count, async=None, timeout=None):
        return self._send_agent_rpc(mac, AgentRequest.START_X_VM, args={'name': name, 'count': count},
                                    async=async, timeout=timeout)

    def stop_x_vm(self, mac, name, count, async=None, timeout=None):
        return self._send_agent_rpc(mac, AgentRequest.STOP_X_VM, args={'name': name, 'count': count},
                                    async=async, timeout=timeout)

    def soft_reset(self, mac, async=None, timeout=None):
        return self._send_agent_rpc(mac, AgentRequest.SOFT_RESET, async=async, timeout=timeout)
