#!/usr/bin/env python

import argparse
import logging
import psutil
import random
import time

from copy import deepcopy
from pprint import pprint, pformat
from threading import Thread

from assemblyline.al.common import forge, log as al_log
from assemblyline.al.common.message import MT_ORCHESTRATORHEARTBEAT, Message
from assemblyline.al.common.queue import CommsQueue
from assemblyline.al.core.agents import ServiceAgentClient
from assemblyline.al.service.list_queue_sizes import get_service_queue_lengths
from assemblyline.common.net import get_hostip, get_mac_for_ip, get_hostname

# Preload all orchestrator configs
config = forge.get_config()
CLOCK_TICK = config.core.orchestrator.clock_tick
GRACE_TICKS_UP = config.core.orchestrator.grace_ticks.up
GRACE_TICKS_DOWN = config.core.orchestrator.grace_ticks.down
HEARTBEAT_INTERVAL = config.system.update_interval
MIN_SVC_WORKERS = config.core.orchestrator.min_service_workers
NODE_REFRESH_TICKS = config.core.orchestrator.node_refresh_ticks
OVER_PROVISION_PCT = config.core.orchestrator.over_provision_pct
SPAWN_COUNT_UP = config.core.orchestrator.spawn_count.up
SPAWN_COUNT_DOWN = config.core.orchestrator.spawn_count.down
TARGET_USAGE = config.core.orchestrator.target_usage
THRESHOLD_COUNT_UP = config.core.orchestrator.thresholds_cnt.up
THRESHOLD_COUNT_DOWN = config.core.orchestrator.thresholds_cnt.down


DATASTORE = forge.get_datastore()
DEFAULT_SERVICE_TIMEOUT = 60
NODES = {}
SERVICE_MAP = {}
VERBOSE = False
VM_MAP = {}

RPC_TIMEOUT = 10
RPC_RESET_TIMEOUT = 120
SERVICE_AGENT_CLIENT = ServiceAgentClient(sender='orchestrator', async=False, timeout=RPC_TIMEOUT)

CURRENT_RESOURCES_PCT = {
    'cpu': 0,
    'ram': 0
}

# [DONE] 1.  Do no stop services if none of them needs more processing (keep status-quo)
# [DONE] 2.  Report cluster core usage
# [DONE] 3.  Pick node randomly based on available cores
# [DONE] 4.  Make sure a given service does not allocate more then half the cluster size
# [DONE] 5.  Listen to SvcHearbeats to know if node allocation mismatches with orchestrator
# [DONE] 6.  Send start/stop commands directly to hostagent with acknowledge messages
# [DONE] 7.  Set maximum allocation per service (licence requirement)
# [DONE] 8.  Create installer for orchestrator
# [DONE] 9.  Rip out old code (profiles, provisioning, etc..)
# TODO:  10. Create upgrade script for prod_3.2 to orchestrator
# [DONE] 11. Dispatcher should auto reload service list
# [DONE] 12. Dashboard should auto reload service list
# TODO:  13. Load factor based proportional distribution


class CannotAllocateService(Exception):
    pass


class NoNodesReady(Exception):
    pass


class VM(object):
    def __init__(self, data):
        self.name = data['name']
        self.num_workers = data['num_workers']
        self.ram = data['ram']
        self.core = data['vcpus']
        
        
class Service(object):
    def __init__(self, data):
        self.name = data['name']
        self._core = data['core']
        self._licence_count = data['licence_count']
        self._ram = data['ram']
        self.timeout = data['timeout']

        self.needed = 0
        self.nodes = []
        self.ticks_down = 0
        self.ticks_up = 0
       
    @property
    def is_vm(self):
        return self.name in VM_MAP
        
    @property
    def core(self):
        if self.is_vm:
            return VM_MAP[self.name].core
        else:
            return self._core

    @property
    def ram(self):
        if self.is_vm:
            return VM_MAP[self.name].ram
        else:
            return self._ram

    @property
    def licence_count(self):
        if self.is_vm:
            vm_count = VM_MAP[self.name].num_workers
            return (int(self._licence_count) / int(vm_count)) * int(vm_count)
        else:
            return self._licence_count

    @property
    def min_svc_workers(self):
        if self.is_vm:
            vm_count = VM_MAP[self.name].num_workers
            return int(max(1, round(MIN_SVC_WORKERS / (vm_count * 1.0))) * vm_count)
        else:
            return MIN_SVC_WORKERS

    @property
    def spawn_count_up(self):
        if self.is_vm:
            vm_count = VM_MAP[self.name].num_workers
            return int(max(1, round(SPAWN_COUNT_UP / (vm_count * 1.0))) * vm_count)
        else:
            return SPAWN_COUNT_UP
    
    @property
    def spawn_count_down(self):
        if self.is_vm:
            vm_count = VM_MAP[self.name].num_workers
            return int(max(1, round(SPAWN_COUNT_DOWN / (vm_count * 1.0))) * vm_count)
        else:
            return SPAWN_COUNT_DOWN

    @property
    def svc_up_threshold(self):
        return ((THRESHOLD_COUNT_UP*1.0)/self.timeout)*DEFAULT_SERVICE_TIMEOUT

    @property
    def svc_down_threshold(self):
        return ((THRESHOLD_COUNT_DOWN*1.0)/self.timeout)*DEFAULT_SERVICE_TIMEOUT

    # noinspection PyProtectedMember
    def update(self, new_service):
        self._core = new_service._core
        self._licence_count = new_service._licence_count
        self._ram = new_service._ram
        self.timeout = new_service.timeout
        
        
class Node(object):
    def __init__(self, data):
        self.mac = data['mac']
        self.ip = data['ip']
        self.cores = data['cores']
        self.memory = data['memory']
        self.running_services = []
        self.used_cores = 0
        self.used_memory = 0


def send_heartbeat():
    global CURRENT_RESOURCES_PCT

    try:
        ip = get_hostip()
        hostinfo = {
            'ip:': get_hostip(),
            'mac_address': get_mac_for_ip(ip),
            'host': get_hostname(),
        }

        while True:
            if VERBOSE:
                log.info('Sending heartbeat.')

            total_cores, total_memory, used_cores, used_memory = get_resources_usage()

            registered_nodes = [
                node for node in DATASTORE.get_nodes(DATASTORE.list_node_keys())
                if node is not None and 'hostagent' in node.get('roles', []) and node.get('enabled', False)
            ]

            CURRENT_RESOURCES_PCT = {
                'cpu': used_cores/total_cores*100,
                'ram': used_memory/total_memory*100
            }

            heartbeat = {
                'resources': {
                    "cores": {
                        'total': total_cores,
                        'used': used_cores
                    },
                    "memory": {
                        'total': total_memory,
                        'used': used_memory
                    },
                    "node_count": len(NODES),
                    "registered_node_count": len(registered_nodes),
                    "cpu_usage.percent": psutil.cpu_percent(),
                    "mem_usage.percent": psutil.virtual_memory().percent,
                    "disk_usage.percent": psutil.disk_usage('/').percent,
                    "disk_usage.free": psutil.disk_usage('/').free,
                },
                'nodes': {k: v.__dict__ for k, v in NODES.iteritems()},
                'vms': {k: v.__dict__ for k, v in VM_MAP.iteritems()},
                'services': {k: v.__dict__ for k, v in SERVICE_MAP.iteritems()},
                'hostinfo': hostinfo
            }
            msg = Message(to='*', mtype=MT_ORCHESTRATORHEARTBEAT, sender='orchestrator', body=heartbeat)
            CommsQueue('status').publish(msg.as_dict())
            time.sleep(HEARTBEAT_INTERVAL)
    except KeyboardInterrupt:
        print 'Exiting.'
    

def get_resources_usage():
    cores = 0
    memory = 0
    total_cores = 0
    total_memory = 0

    for _, host in NODES.iteritems():
        cores += host.used_cores
        memory += host.used_memory

        total_cores += host.cores
        total_memory += host.memory

    return total_cores or 0.01, total_memory or 1, cores, memory


def refresh_vm_map():
    global VM_MAP
    log.info("Refreshing VM map")
    VM_MAP = {x['name']: VM(x) for x in DATASTORE.list_virtualmachines()}


def refresh_service_map():
    global SERVICE_MAP

    log.info("Refreshing service map")
    temp_service_map = {
        srv['name']: Service({'name': srv['name'],
                              'core': srv.get('cpu_cores', 1),
                              'licence_count': srv.get('licence_count', 0),
                              'ram': srv.get('ram_mb', 256),
                              'timeout': srv.get('timeout', DEFAULT_SERVICE_TIMEOUT)})
        for srv in DATASTORE.list_services() if srv['enabled']
    }

    for name, svc_data in temp_service_map.iteritems():
        if name not in SERVICE_MAP:
            SERVICE_MAP[name] = svc_data
        else:
            SERVICE_MAP[name].update(svc_data)

    if VERBOSE:
        pprint(SERVICE_MAP)

    # Go through list of SERVICE_MAP and if they don't show up in temp_service_list deallocate the services
    to_delete_svc = []
    for name in SERVICE_MAP.keys():
        if name not in temp_service_map:
            stop_service(name, len(SERVICE_MAP[name].nodes))
            to_delete_svc.append(name)

    # Remove services that are no longer relevant
    for name in to_delete_svc:
        del SERVICE_MAP[name]


def _deallocate_node(mac):
    global SERVICE_MAP, NODES

    log.warning("Removing nodes '%s' instances from all services" % mac)
    for svc in SERVICE_MAP.keys():
        while mac in SERVICE_MAP[svc].nodes:
            SERVICE_MAP[svc].nodes.remove(mac)

    _reset_node(mac)

    if mac in NODES:
        del NODES[mac]


def refresh_node_map(restore_usage=False):
    global NODES, SERVICE_MAP

    log.info("Refreshing node map")
    modified = False

    op_mult = 1
    if OVER_PROVISION_PCT > 0:
        op_mult = (OVER_PROVISION_PCT / 100) + 1

    node_list = {node['mac_address']: Node({'cores': float(node['machine_info']['cores']) * op_mult,
                                            'ip': node['ip'],
                                            'mac': node['mac_address'],
                                            'memory': float(node['machine_info']['memory']) * 1024})
                 for node in DATASTORE.get_nodes(DATASTORE.list_node_keys())
                 if node is not None and 'hostagent' in node.get('roles', [])
                 and node.get('enabled', False)}

    # Add new nodes to the node list
    for mac, node in node_list.iteritems():
        # Check if node is up
        hb = SERVICE_AGENT_CLIENT.heartbeat(mac, async=False, timeout=2)
        if not isinstance(hb, dict):
            # Node is down
            if mac in NODES:
                _deallocate_node(mac)
            continue

        if mac not in NODES:
            # This is a new node
            log.info("Node '%s' is ready to be tasked!" % mac)
            NODES[mac] = node
            
        # Check if node is out of sync with orchestrator
        outsync_svc_list = deepcopy(NODES[mac].running_services)
        extra_svc_list = []
        for svc_name, details in hb.get('services', {}).get('details', {}).iteritems():
            for _ in xrange(details['num_workers']):
                try:
                    outsync_svc_list.remove(svc_name)
                except ValueError:
                    extra_svc_list.append(svc_name)

        hb_vm_map = hb.get('vmm', {}) or {}
        for vm_name in hb_vm_map.keys():
            svc_name = vm_name.split(".")[0]
            try:
                outsync_svc_list.remove(svc_name)
            except ValueError:
                extra_svc_list.append(svc_name)

        if len(outsync_svc_list) > 0:
            modified = True
            log.warning("Following services are missing on node '%s':  %s" % (mac, ", ".join(outsync_svc_list)))
            # Remove out of sync services
            try:
                for svc in outsync_svc_list:
                    NODES[mac].used_cores -= SERVICE_MAP[svc].core
                    NODES[mac].used_memory -= SERVICE_MAP[svc].ram
                    NODES[mac].running_services.remove(svc)

                    SERVICE_MAP[svc].nodes.remove(mac)
            except KeyError:
                _deallocate_node(mac)
                NODES[mac] = node
                continue

        if len(extra_svc_list) > 0:
            modified = True
            log.warning("Following services are running extra on node '%s':  %s" % (mac, ", ".join(extra_svc_list)))
            # Remove out of sync services
            try:
                for svc in extra_svc_list:
                    NODES[mac].used_cores += SERVICE_MAP[svc].core
                    NODES[mac].used_memory += SERVICE_MAP[svc].ram
                    NODES[mac].running_services.append(svc)

                    if restore_usage:
                        SERVICE_MAP[svc].needed += 1
                    SERVICE_MAP[svc].nodes.append(mac)
            except KeyError:
                _deallocate_node(mac)
                NODES[mac] = node
                continue

    # Go through list of NODES and mark them to be removed if they don't show up in node_list
    to_delete_nodes = []
    for mac in NODES.keys():
        if mac not in node_list:
            to_delete_nodes.append(mac)

    # Deallocate nodes that are no longer relevant
    for mac in to_delete_nodes:
        _deallocate_node(mac)

    return modified


def assign_service_to_node(svc, force_apply):
    svc_core = SERVICE_MAP[svc].core
    svc_ram = SERVICE_MAP[svc].ram

    if not force_apply:
        potential_nodes = [(mac, node, int(min((node.cores - node.used_cores) / svc_core,
                                           (node.memory - node.used_memory) / svc_ram)))
                           for mac, node in NODES.iteritems()
                           if (node.cores - node.used_cores) / svc_core >= 1
                           and (node.memory - node.used_memory) / svc_ram >= 1]
        if len(NODES) == 0:
            raise NoNodesReady("No nodes are ready in your cluster")
        elif not potential_nodes:
            raise CannotAllocateService("Could not allocate service: %s" % svc)

        random_range = 0
        for _, _, range_count in potential_nodes:
            random_range += range_count

        selected = random.randint(1, random_range)

        selected_range = 0
        for (mac, node, select_count) in potential_nodes:
            selected_range += select_count
            if selected_range >= selected:
                return mac

        raise CannotAllocateService("Could not allocate service: %s" % svc)
    else:
        potential_nodes = sorted([(mac, node, int(min((node.cores - node.used_cores) / svc_core,
                                                  (node.memory - node.used_memory) / svc_ram)))
                                  for mac, node in NODES.iteritems()], key=lambda tup: tup[2], reverse=True)

        if not potential_nodes:
            raise NoNodesReady("No nodes are ready in your cluster")

        selected_mac, selected_node, _ = potential_nodes[0]

        return selected_mac


def _hostagent_start(name, mac):
    if SERVICE_MAP[name].is_vm:
        ret_val = SERVICE_AGENT_CLIENT.start_x_vm(mac, name, 1)
    else:
        ret_val = SERVICE_AGENT_CLIENT.start_x_service(mac, name, 1)

    if ret_val != 'started':
        log.warning("Start vm/service RPC command failed on node '%s': %s" % (mac, ret_val))
        return False
    return True


def start_service(name, count, force_apply_count=0):
    failed = 0

    for x in xrange(count):
        try:
            force_apply = x < force_apply_count
            selected_mac = assign_service_to_node(name, force_apply)
            msg = "Starting service '%s' on node '%s'" % (name, selected_mac)
            if force_apply:
                msg += " [FORCED]"
            log.info(msg)

            # Take action
            if not _hostagent_start(name, selected_mac):
                log.warning("Could not allocate service '%s' on node '%s'" % (name, selected_mac))
                refresh_node_map()
                break

            # Adjust NODE and SERVICE_MAP
            NODES[selected_mac].used_cores += SERVICE_MAP[name].core
            NODES[selected_mac].used_memory += SERVICE_MAP[name].ram
            NODES[selected_mac].running_services.append(name)
            
            SERVICE_MAP[name].nodes.append(selected_mac)

        except CannotAllocateService:
            failed += 1

    if failed:
        log.info("Could not allocate %sx %s service." % (failed, name))


def _hostagent_stop(name, mac):
    if SERVICE_MAP[name].is_vm:
        ret_val = SERVICE_AGENT_CLIENT.stop_x_vm(mac, name, 1)
    else:
        ret_val = SERVICE_AGENT_CLIENT.stop_x_service(mac, name, 1)

    if ret_val != 'stopped':
        log.warning("Stop vm/service RPC command failed on node '%s': %s" % (mac, ret_val))
        return False
    return True


def stop_service(name, count):
    for x in xrange(count):
        try:
            random_mac = random.choice(SERVICE_MAP[name].nodes)
        except IndexError:
            log.error("Trying to deallocate service '%s' but it's "
                      "not allocated on any nodes: \n%s" % (name, pformat(SERVICE_MAP[name])))
            return
        log.info("Stopping service '%s' on node '%s'" % (name, random_mac))

        # Take action
        if not _hostagent_stop(name, random_mac):
            log.warning("Could not remove service '%s' from node '%s'" % (name, random_mac))
            refresh_node_map()
            break

            # Adjust NODE and SERVICE_MAP
        NODES[random_mac].used_cores -= SERVICE_MAP[name].core
        NODES[random_mac].used_memory -= SERVICE_MAP[name].ram
        NODES[random_mac].running_services.remove(name)

        SERVICE_MAP[name].nodes.remove(random_mac)
        

def _reset_node(mac):
    log.info("Resetting node '%s' services and VMs" % mac)

    pong = SERVICE_AGENT_CLIENT.ping(mac, async=False, timeout=1)
    if pong != "PONG":
        log.info("Node '%s' already down" % mac)
        return

    log.info("Reset configuration on node '%s'" % mac)
    ret_val = SERVICE_AGENT_CLIENT.soft_reset(mac, timeout=RPC_RESET_TIMEOUT)
    if not ret_val:
        log.warning("Soft_reset RPC command failed on node '%s': %s" % (mac, ret_val))


# noinspection PyBroadException
def run_forever():
    global SERVICE_MAP, NODES
    run = True
    node_ticks = 0

    log.info("Orchestrator is staring up")

    refresh_vm_map()
    refresh_service_map()
    refresh_node_map(restore_usage=True)

    service_map_restore_needed = {k: v.needed for k, v in SERVICE_MAP.iteritems()}

    while run:
        out_of_sync = False
        apply_changes = False
        take_down = False

        try:
            # Refresh node list
            if node_ticks >= NODE_REFRESH_TICKS:
                log.info("Syncing nodes and services")
                node_ticks = 0
                refresh_vm_map()
                refresh_service_map()
                out_of_sync = refresh_node_map()
            node_ticks += 1

            if not out_of_sync:
                # Get service queue lengths
                queues = get_service_queue_lengths()

                # Create a restore point if no actions are required
                service_map_restore_needed = {k: v.needed for k, v in SERVICE_MAP.iteritems()}

                # Check all queues
                for name, q_len in queues.iteritems():
                    if name not in SERVICE_MAP:
                        # Service is disabled
                        if q_len > 0:
                            log.warning("Service '%s' as a non-zero queue but "
                                        "orchestrator thinks it's disabled" % name)
                        continue

                    svc_data = SERVICE_MAP[name]

                    # Adjust needed amount of services
                    if svc_data.needed > len(svc_data.nodes) and q_len <= svc_data.svc_down_threshold:
                        # 0. We could not provision enough of a given service during last run but
                        #    now it's queue has gone back to the service down threshold. Lets stop
                        #    trying to provision more...
                        svc_data.needed = len(svc_data.nodes)

                    if len(svc_data.nodes) != svc_data.needed:
                        # 1. Service requirements were not fully assigned during the last run
                        #    therefor changes will be applied no matter what.
                        if svc_data.needed < len(svc_data.nodes):
                            svc_data.needed = len(svc_data.nodes)

                        apply_changes = True

                    if len(svc_data.nodes) < svc_data.min_svc_workers:
                        # 2. Service does not meet the minimum service requirements

                        apply_changes = True
                        svc_data.needed += svc_data.min_svc_workers
                        svc_data.ticks_up = 0
                        svc_data.ticks_down = 0

                    elif len(svc_data.nodes) == 0 and q_len > 0 and svc_data.needed == 0:
                        # 3. There is a job in the queue and no services are loaded
                        #    NOTE: This is the case where min_svc_worker is set to 0, we don't want to wait
                        #          for the service to reach the up threshold, we need to allocate a service now
                        apply_changes = True
                        svc_data.needed += svc_data.spawn_count_up
                        svc_data.ticks_up = 0
                        svc_data.ticks_down = 0

                    elif q_len >= svc_data.svc_up_threshold and not svc_data.needed > len(svc_data.nodes):
                        # 4. Service queue meets the threshold for allocating more instances
                        if VERBOSE:
                            log.info("- tick_up '%s': %s" % (name, svc_data.ticks_up))
                        svc_data.ticks_down = 0
                        if svc_data.ticks_up == GRACE_TICKS_UP:
                            # There is at least one service that needs more
                            # processing power, SERVICE_MAP will be applied
                            apply_changes = True
                            svc_data.needed += svc_data.spawn_count_up
                            svc_data.ticks_up = 0
                        else:
                            svc_data.ticks_up += 1

                    elif q_len <= svc_data.svc_down_threshold:
                        # 5. Service queue meets the threshold for de-allocating some instances
                        if VERBOSE:
                            log.info("- tick_down '%s': %s" % (name, svc_data.ticks_down))
                        svc_data.ticks_up = 0
                        if svc_data.ticks_down == GRACE_TICKS_DOWN:
                            take_down = True
                            svc_data.needed = max(svc_data.needed - svc_data.spawn_count_down, svc_data.min_svc_workers)
                            svc_data.ticks_down = 0
                        else:
                            svc_data.ticks_down += 1
                    else:
                        # 6. Service queue is between thresholds
                        if VERBOSE:
                            log.info("= reset '%s'" % name)
                        svc_data.ticks_up = 0
                        svc_data.ticks_down = 0

                    if svc_data.licence_count and svc_data.needed > svc_data.licence_count:
                        log.warning("Service '%s' licence is maxed out. "
                                    "Cannot allocate more then %s instances" % (name, svc_data.licence_count))

                        apply_changes = True
                        svc_data.needed = svc_data.licence_count

            if take_down and TARGET_USAGE < max(CURRENT_RESOURCES_PCT['cpu'], CURRENT_RESOURCES_PCT['ram']):
                apply_changes = True

            # Should we apply all the changes we've made to the service map?
            if apply_changes or out_of_sync:
                # De-allocated all extra service first
                for name, svc_data in SERVICE_MAP.iteritems():
                    if svc_data.needed < len(svc_data.nodes):
                        stop_service(name, len(svc_data.nodes) - svc_data.needed)
                    else:
                        if VERBOSE:
                            log.info("Nothing to do for service '%s'" % name)

                # Allocate new services using spare space (Sorted by busiest service)
                services = sorted([(name, data) for name, data in SERVICE_MAP.iteritems()],
                                  key=lambda s: queues.get(s[0], 0), reverse=True)
                for name, svc_data in services:
                    if svc_data.needed > len(svc_data.nodes):
                        start_service(name, svc_data.needed - len(svc_data.nodes),
                                      max(0, svc_data.min_svc_workers - len(svc_data.nodes)))
            else:
                # Since we should not apply the changes, we've will restore the map backup
                for key in SERVICE_MAP.keys():
                    SERVICE_MAP[key].needed = service_map_restore_needed[key]

            # Sleep for clock tick
            time.sleep(CLOCK_TICK)
        except NoNodesReady as nnr:
            for key in SERVICE_MAP.keys():
                SERVICE_MAP[key].needed = service_map_restore_needed[key]

            log.error(nnr.message)
            node_ticks = NODE_REFRESH_TICKS
            time.sleep(CLOCK_TICK)
        except Exception as e:
            log.exception(e.message)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="orchestrator",
                                     description="Launch Assemblyline's central cluster management process")
    parser.add_argument('-v', '--verbose', action='store_true', help='Show verbose output', default=False)
    args = parser.parse_args()

    al_log.init_logging("orchestrator")
    log = logging.getLogger('assemblyline.orchestrator')

    if OVER_PROVISION_PCT != 0:
        log.info('Orchestrator allows over provisioning up to %s%%.' % OVER_PROVISION_PCT)

    VERBOSE = args.verbose

    if VERBOSE:
        log.setLevel(logging.DEBUG)

    # Starting hearbeat thread
    t = Thread(target=send_heartbeat)
    t.daemon = True
    t.start()

    run_forever()
