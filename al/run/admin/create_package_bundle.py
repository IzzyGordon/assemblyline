#!/usr/bin/env python

import os


def parse_param(p, od, slices):
    if (p.startswith("'") and p.endswith("'")) or (p.startswith('"')) and p.endswith('"'):
        return p[1:-1]
    elif "join(" in p:
        join_items = p.split('join(')[1].split(',')
        return "/".join([parse_param(j_i.strip().replace(")", ""), od, slices) for j_i in join_items])
    elif "+" in p:
        join_items = p.split('+')
        return "/".join([parse_param(j_i.strip(), od, slices) for j_i in join_items]).replace("//", "/")
    elif ".format(" in p:
        pattern, items = p.split('.format(', 1)
        args = []
        for item in items.split(","):
            args.append(parse_param(item.strip().replace(")", ""), od, slices))
        pattern = parse_param(pattern.strip(), od, slices)
        return pattern.format(*args)
    elif " % " in p:
        pattern, items = p.split(' % ', 1)
        args = []
        for item in items.split(","):
            args.append(parse_param(item.strip().replace(")", "").replace("(", ""), od, slices))
        if len(args) == 1:
            args = args[0]
        else:
            raise Exception("Can't parse yet: %s" % p)
        pattern = parse_param(pattern.strip(), od, slices)
        return pattern % args
    else:
        for offset, max_search in slices:
            od_slice = od[offset:max_search]

            if (p + " = ") in od_slice:
                values = od_slice.split(p + " = ")
            elif (p + "=") in od_slice:
                values = od_slice.split(p + "=")
            else:
                continue

            find_val = None
            if len(values) >= 1 and values[0][-1] in ["\t", " ", "\n"]:
                v = values[1].strip()
                find_val = v.split("\n")[0].strip()
            if find_val:
                return parse_param(find_val, od, slices)

        raise Exception("Can't parse yet: %s" % p)


# noinspection PyBroadException
def extract_package_paths(data):
    paths = []
    splitted = data.split(".fetch_package(")
    max_search = len(splitted[0])
    slices = [(0, max_search)]
    for item in splitted[1:]:
        end_items = item.split(")")
        to_join = []
        for end_item in end_items:
            to_join.append(end_item)
            if "(" not in end_item:
                break

        fetch_params = ")".join(to_join)

        param_items = fetch_params.split(",")
        p1_parts = []
        for p_item in param_items:
            p1_parts.append(p_item)
            if "(" not in p_item:
                break
            if ")" in p_item:
                break

        param = ",".join(p1_parts)
        if param.startswith("r'") or param.startswith('r"') or param.startswith("u'") or param.startswith('u"'):
            param = param[1:]

        try:
            paths.append(parse_param(param, data, slices))
        except Exception:
            print "ERR: Cannot parse param [%s]" % param
        offset = max_search + 15
        max_search = offset + len(item)
        slices.insert(0, (offset, max_search))
    return paths


def list_packages():
    output = []
    path = os.path.realpath(__file__)
    path = path[:os.path.realpath(__file__).find("assemblyline")]
    for root, _, files in os.walk(path):
        for f in files:
            if f.endswith(".py") and f != "create_package_bundle.py":
                with open(os.path.join(root, f), "rb") as fh:
                    python_script = fh.read()
                    if ".fetch_package(" in python_script:
                        output.extend(extract_package_paths(python_script))
    return output


def list_git_repos():
    output = []
    path = os.path.realpath(__file__)
    path = path[:os.path.realpath(__file__).find("assemblyline")]

    for root, dirs, _ in os.walk(path):
        for d in dirs:
            if d == ".git":
                output.append(root)
    return output


if __name__ == "__main__":
    import argparse
    import sys
    sys.path.append(os.path.dirname(os.path.realpath(__file__)).replace("assemblyline/al/run/admin", ""))

    parser = argparse.ArgumentParser()
    parser.add_argument('output', default='~/al_bundles', help='Directory to write bundle to.', nargs='?')
    parser.add_argument('--seed', default='assemblyline.al.install.seeds.assemblyline_common.seed',
                        help='Assemblyline install seed classpath for package source details.')
    args = parser.parse_args()

    output_dir = args.output

    if "~" in output_dir:
        output_dir = os.path.expanduser(output_dir)
    output_dir = os.path.abspath(output_dir)
    work_dir = os.path.join(output_dir, "tmp")

    from assemblyline.al.install import SiteInstaller
    alsi = SiteInstaller(seed=args.seed)

    alsi.milestone('Gathering git repositories...')
    git_repos = list_git_repos()
    alsi.info(git_repos)

    alsi.milestone('Creating bundles...')
    alsi.info('Creating working directory...')
    alsi.runcmd("mkdir -p %s" % work_dir, shell=True)

    for r in git_repos:
        repo = os.path.basename(r)
        try:
            alsi.runcmd("git bundle create %s.prod_3.2 prod_3.2" % os.path.join(work_dir, repo), cwd=r, shell=True)
        except Exception:
            alsi.warn("Cannot create git bundle for repo: %s" % repo)
            alsi.runcmd("rm %s.prod_3.2" % os.path.join(work_dir, repo), shell=True, raise_on_error=False)

    alsi.info("Creating tar archive...")
    git_bundle_file = os.path.join(output_dir, "git_bundles.tgz")
    alsi.runcmd("tar zcf %s -C %s --xform 's:^\./::' ." % (git_bundle_file, work_dir), shell=True)

    alsi.info("Removing temp working directory...")
    alsi.runcmd("rm -rf %s" % work_dir, shell=True)
    alsi.milestone("Git bundle created: %s" % git_bundle_file)

    alsi.milestone('Gathering packages list...')
    pkg_list = list_packages()
    alsi.info(pkg_list)
    if pkg_list:
        alsi.milestone('Processing files...')
        for pkg in pkg_list:
            # noinspection PyBroadException
            try:
                alsi.fetch_package(pkg, os.path.join(work_dir, pkg))
            except KeyboardInterrupt:
                exit(1)
            except Exception:
                alsi.warn("PKG '%s' not found." % pkg)

        alsi.milestone("Creating bundle...")

        alsi.info("Removing empty directories...")
        alsi.runcmd("find %s -type d -empty -delete" % work_dir, shell=True)

        if not os.path.exists(work_dir):
            alsi.error("No files were retrieved for bundle, are you using the right seed?")
            exit(1)

        alsi.info("Creating tar archive...")
        bundle_file = os.path.join(output_dir, "package_bundle.tgz")
        alsi.runcmd("tar zcf %s -C %s --xform 's:^\./::' ." % (bundle_file, work_dir), shell=True)

        alsi.info("Removing temp working directory...")
        alsi.runcmd("rm -rf %s" % work_dir, shell=True)

        alsi.milestone("Package bundle created: %s" % bundle_file)
    else:
        alsi.milestone("Done but no files to download...")
