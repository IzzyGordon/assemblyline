
from assemblyline.common.importing import class_by_name
import re
import copy


def infer_classpath(service):
    """Guess the classpath from a potentially incomplete service config.

    This method should only be used in install processes.
    """
    try:
        return service['classpath']
    except KeyError:
        return "al_services.%s.%s" % (service['repo'], service['class_name'])


def _bool_type(value):
    """A slightly stricter bool cast than the built in type function."""
    if type(value) in [int, bool]:
        return bool(value)

    elif type(value) in [str, unicode]:
        if str(value).lower() == 'true':
            return True
        if str(value).lower() == 'false':
            return False

    raise ValueError()


_bool_type.__name__ = 'bool'

# Keys a service must have,
# name, type
__require_service_tags = [
    ['class_name', str],
    ['classpath', str],
    ['config', dict],
    ['enabled', _bool_type],
    ['repo', str],
]

# Keys that a service may or may not have,
# name, type
__optional_service_tags = [
    ['licence_count', int],
    ['accepts', str],
    ['category', str],
    ['cpu_cores', float],
    ['description', str],
    ['ram_mb', float],
    ['rejects', str],
    ['stage', str],
    ['submission_params', list],
    ['supported_platforms', list],
    ['timeout', float],
    ['name', str],
    ['is_external', _bool_type],
    ['install_by_default', _bool_type],
    ['realm', str],
]


def validate_service(service):
    """Try to ensure a service configuration is minimally correct."""
    service = copy.deepcopy(service)

    # If the classpath is not set try to infer it from the repo and the class name
    if 'classpath' not in service:
        if 'repo' not in service or 'class_name' not in service:
            raise ValueError("A service may not be specified missing 'classpath' and both 'repo' and 'class_name'.")
        service['classpath'] = "al_services.%s.%s" % (service['repo'], service['class_name'])

    # If the classpath is set, but the repo or class name are missing try to fill them in
    elif 'repo' not in service or 'class_name' not in service:
        pattern = r'al_services.(?P<repo>alsvc_\w+).(?P<class_name>\w+)'
        matching = re.match(pattern, service['classpath'])
        if matching is None:
            raise ValueError("A service was missing 'repo' or 'class_name', "
                             "and they could not be inferred from 'classpath'")
        service['repo'] = service.get('repo', matching.group('repo'))
        service['class_name'] = service.get('class_name', matching.group('class_name'))

    # Check for required keys
    failed_keys = []
    for name, type_fn in __require_service_tags:
        try:
            service[name] = type_fn(service[name])
        except KeyError:
            failed_keys.append(name + ' is missing, expected ' + str(type_fn.__name__))
        except ValueError:
            failed_keys.append(name + ' must be a ' + str(type_fn.__name__))

    # Check for optional keys
    for name, type_fn in __optional_service_tags:
        try:
            service[name] = type_fn(service[name])
        except KeyError:
            pass  # Optional keys may be missing
        except ValueError:
            failed_keys.append(name + ' must be a ' + str(type_fn.__name__))

    # Check if any tests failed
    if len(failed_keys) > 0:
        raise ValueError('There are errors in the format of this service configuration: ' + '; '.join(failed_keys))

    return service


def get_merged_svc_config(name, configuration, log):
    if 'config' in configuration:
        config_overrides = configuration.pop('config')
    else:
        config_overrides = {}

    try:
        configuration['classpath'] = infer_classpath(configuration)
        cls = class_by_name(configuration['classpath'])
        if not hasattr(cls, "get_default_config"):
            log.error(name + " is not an AL service. Make sure the class path you've entered is valid.")
            return configuration
    except (ImportError, KeyError):
        # log.error(classpath + " could not be found. Make sure the class path you've entered is valid.")
        configuration['config'] = config_overrides
        return configuration

    cfg = cls.get_default_config()
    cfg.update(configuration)

    if config_overrides:
        for cfg_key, cfg_value in config_overrides.iteritems():
            if cfg_key not in cfg['config'] and cfg_key != 'PLUMBER_MAX_QUEUE_SIZE':
                log.warn("Config override %s is not a valid configuration option for %s" % (cfg_key, name))
                continue
            cfg['config'][cfg_key] = cfg_value

    return cfg
