DEFAULT_REGISTRATION = {
        'ip': '',
        'hostname': '',
        'mac_address': "",
        'enabled': True,
        'is_vm': False,
        'vm_host': ''
}

DEFAULT_CORE_REGISTRATION = {
    'enabled': True,
    'hostname': '',
    'ip': '',
    'mac_address': '',
    'machine_info': {},
    'platform': {}
}
