
import copy
import pytest
from assemblyline.al.common.service_utils import infer_classpath
from assemblyline.al.common.service_utils import validate_service


def test_infer_classpath():
    # classpath should be preferred key
    assert infer_classpath({'classpath': 'abc', 'repo': 'aaa', 'class_name': 'bbb'}) == 'abc'

    # repo and class_name should be used otherwise
    assert infer_classpath({'repo': 'aaa', 'class_name': 'bbb'}) == 'al_services.aaa.bbb'

    # One or the other is required
    with pytest.raises(KeyError):
        infer_classpath({})
    with pytest.raises(KeyError):
        infer_classpath({'class_name': 'bbb'})
    with pytest.raises(KeyError):
        infer_classpath({'repo': 'aaa'})


service_config_sample = {
    "class_name": "Extract",
    "classpath": "al_services.alsvc_extract.Extract",
    "config": {},
    "enabled": True,
    "name": "Extract",
    "repo": "alsvc_extract",
}


def test_validate_config_valid():
    # The sample is valid, and shouldn't change
    before = copy.deepcopy(service_config_sample)
    out = validate_service(before)
    assert out == service_config_sample
    assert before == service_config_sample
    assert id(before) != id(out)


def test_validate_config_required():
    before = copy.deepcopy(service_config_sample)
    del before['enabled']
    with pytest.raises(ValueError):
        validate_service(before)


def test_validate_config_type():
    before = copy.deepcopy(service_config_sample)
    before['timeout'] = 'yes'
    with pytest.raises(ValueError):
        validate_service(before)


def test_validate_config_classpath():
    service = copy.deepcopy(service_config_sample)
    del service['classpath']
    service = validate_service(service)
    assert 'classpath' in service

    del service['repo']
    service = validate_service(service)
    assert 'repo' in service

    del service['class_name']
    service = validate_service(service)
    assert 'class_name' in service

    del service['repo']
    del service['class_name']
    service = validate_service(service)
    assert 'repo' in service and 'class_name' in service
