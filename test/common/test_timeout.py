
from assemblyline.common import timeout

import unittest
import time
import subprocess


def run_sp_timer(timer, shell_func):
    proc = timer.run(subprocess.Popen(shell_func, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE))
    proc.wait()
    timer.close()
    return not timer.has_timed_out()


class TestSubprocessTimer(unittest.TestCase):
    def test_bare(self):
        """In the following, we initialize a timer and and use it multiple times"""
        st = timeout.SubprocessTimer(3)
        self.assertFalse(run_sp_timer(st, 'sleep 5'))
        self.assertTrue(run_sp_timer(st, 'sleep 1'))
        self.assertFalse(run_sp_timer(st, 'sleep 5'))

    def test_context(self):
        """In the following, we use a timer wrapped into a 'with' statement"""
        # print("\n\n'With statement' timer kills processes running more than 2 seconds...")
        # print("\n-->> Executing commandL: \"sleep 4\"")
        stime = etime = time.time()
        ret_val = None
        with self.assertRaises(timeout.TimeoutException):
            with timeout.SubprocessTimer(2) as new_ST:
                proc = new_ST.run(subprocess.Popen(["sleep", "10"], stderr=subprocess.PIPE, stdout=subprocess.PIPE))
                proc.wait()
                ret_val = proc.poll()
                etime = time.time()

        self.assertLess(etime - stime, 4)
        self.assertGreater(etime - stime, 1.5)
        self.assertNotEqual(ret_val, 0)


def return_after(value, timeout):
    time.sleep(timeout)
    return value


class TestTimeout(unittest.TestCase):
    def test_notimeout(self):
        self.assertEqual(timeout.timeout(return_after, (10, 1), timeout_duration=2), 10)
        self.assertEqual(timeout.timeout(return_after, ("abc", 1), timeout_duration=2), "abc")

    def test_timeout(self):
        with self.assertRaises(BaseException):
            timeout.timeout(return_after, (10, 10), timeout_duration=2)


class TestAlarm(unittest.TestCase):
    def test_noalarm(self):
        with timeout.alarm_clock(1):
            # We won't wait long enough trigger the alarm
            time.sleep(0.5)
        # And the alarm is disabled after the block ends
        time.sleep(1)

    def test_alarm(self):
        with self.assertRaises(timeout.TimeoutException):
            with timeout.alarm_clock(1):
                time.sleep(10)
            self.fail()
