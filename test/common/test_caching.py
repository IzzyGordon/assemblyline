import time
import unittest

from assemblyline.common import caching


class TestCaching(unittest.TestCase):
    def test_timeout(self):
        """Insert a single item and wait for it to expire."""
        tc = caching.TimeExpiredCache(5, 1)
        tc.add(1, 1)

        for _ in range(4):
            time.sleep(1)
            self.assertIsNotNone(tc.get(1))

        time.sleep(2)
        self.assertIsNone(tc.get(1))

    def test_time_order(self):
        """Insert several items and wait for them to expire in order."""
        tc = caching.TimeExpiredCache(5, 0.01)

        for ii in range(5):
            tc.add(ii, ii)
            time.sleep(1)

        time.sleep(0.5)

        tc.add(5, 5)

        for ii in range(5):
            self.assertEqual(tc.get(ii+1), ii+1)
            time.sleep(1)
            self.assertIsNone(tc.get(ii))

    def test_SizeExpiredCache(self):
        """Add elements to the size cache checking that it acts like a queue"""
        sc = caching.SizeExpiredCache(5)
        sequence = [10, 5, 2, 3, 1, 80, -3, 10, 'a', None]

        for item in sequence[:5]:
            sc.add(item, item)

        for index in range(5):
            self.assertEqual(sc.get(sequence[index]), sequence[index])
            self.assertIsNone(sc.get(sequence[index + 5]))

            sc.add(sequence[index + 5], sequence[index + 5])

            self.assertIsNone(sc.get(sequence[index]))
