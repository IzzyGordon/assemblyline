from assemblyline.common import concurrency
import unittest
import time


class TestConcurrency(unittest.TestCase):
    def test_ensure_concurrency(self):
        """Try to evaluate that something is actually concurrent"""
        plan = [(time.sleep, (1, ), key) for key in range(100)]
        start = time.time()
        result = concurrency.execute_concurrently(plan)

        self.assertLess(time.time() - start, 10)
        self.assertEqual(len(result), 100)

    def test_timing(self):
        """Run concurrently with timing"""
        plan = [(time.sleep, (1, ), key) for key in range(100)]

        start = time.time()
        result = concurrency.execute_concurrently(plan, calculate_timers=True)

        self.assertLess(time.time() - start, 20)
        self.assertEqual(len(result), 100 + 1)
        self.assertIn('_timers_', result)
        self.assertEqual(len(result['_timers_']), 100)
        self.assertEqual(set(result['_timers_'].keys()), set(range(100)))

    def test_timeout(self):
        """Run concurrently with timeout set"""
        plan = [(time.sleep, (1, ), key) for key in range(100)]
        plan += [(time.sleep, (100, ), key) for key in range(100, 110)]

        start = time.time()
        result = concurrency.execute_concurrently(plan, max_timeout=2)

        self.assertLess(time.time() - start, 20)
        self.assertEqual(len(result), 100 + 1)
        self.assertIn('_timeout_', result)
        self.assertEqual(len(result['_timeout_']), 10)
        self.assertEqual(set(result['_timeout_']), set(range(100, 110)))

    def test_timing_and_timeout(self):
        """Run concurrently with timing and timeout set"""
        plan = [(time.sleep, (1, ), key) for key in range(100)]
        plan += [(time.sleep, (100, ), key) for key in range(100, 110)]

        start = time.time()
        result = concurrency.execute_concurrently(plan, calculate_timers=True, max_timeout=2)

        self.assertLess(time.time() - start, 20)
        self.assertEqual(len(result), 100 + 2)
        self.assertIn('_timers_', result)
        self.assertEqual(len(result['_timers_']), 100)
        self.assertEqual(set(result['_timers_'].keys()), set(range(100)))
        self.assertIn('_timeout_', result)
        self.assertEqual(len(result['_timeout_']), 10)
        self.assertEqual(set(result['_timeout_']), set(range(100, 110)))

    def test_exception(self):
        """Run concurrently with exception catching"""
        def error(ii):
            raise RuntimeError()

        def other_error(a, b, c):
            time.sleep(a + b + c)

        plan = [(time.sleep, (1, ), key) for key in range(100)]
        plan += [(error, (100, ), key) for key in range(100, 110)]
        plan += [(other_error, (100, ), key) for key in range(100, 120)]

        start = time.time()
        result = concurrency.execute_concurrently(plan)

        self.assertLess(time.time() - start, 20)
        self.assertEqual(len(result), 100 + 1)
        self.assertIn('_exception_', result)
        self.assertEqual(len(result['_exception_']), 20)
        self.assertEqual(set(result['_exception_'].keys()), set(range(100, 120)))


