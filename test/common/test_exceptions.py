import assemblyline.common.exceptions as exceptions
import unittest


class ImproperException(BaseException):
    """An exception that deliberately violates convention and inherits from BaseException directly."""
    pass


class TestExceptions(unittest.TestCase):
    def test_decoractor(self):
        for ex in [exceptions.ChainException, exceptions.RecoverableError, exceptions.NonRecoverableError]:
            @exceptions.chain(ex)
            def test():
                raise RuntimeError()

            with self.assertRaises(ex):
                test()

    def test_execute(self):
        for ex in [exceptions.ChainException, exceptions.RecoverableError, exceptions.NonRecoverableError]:
            chain_caller = exceptions.chain(ex)
            value = []

            def test(arg, kwarg):
                value.append(arg + kwarg)
                raise RuntimeError()

            with self.assertRaises(ex):
                chain_caller.execute(test, 1, kwarg=100)

            self.assertEqual(len(value), 1)
            self.assertEqual(value[0], 101)

    def test_class_decorator(self):
        for ex in [exceptions.ChainException, exceptions.RecoverableError, exceptions.NonRecoverableError]:
            @exceptions.chainall(ex)
            class test:
                def no_throw(self):
                    return 0

                def a(self):
                    # Catch a 'Normal' exception
                    raise RuntimeError()

                def b(self):
                    # NOT Catch something inherited from BaseException
                    raise ImproperException()

            x = test()
            x.no_throw()

            with self.assertRaises(ex):
                x.a()

            with self.assertRaises(ImproperException):
                x.b()
