# Extra steps for offline installation

The following documentation describes extra steps to do to be able to install Assemblyline on an offline network.

There are multiple ways to do this, we will describe two:

 1. Install it while online then switch is offline
 2. Full offline install

## Online then offline

This setup is pretty much strait forward, you run through the normal documentation while the computer is online
then you can disconnect the internet facing network and make your installation offline.

***Note:*** The one thing that you need to keep in mind is that a lot of IP configuration is locked down into
multiple files in the system so you have to make sure that whatever IPs you've chosen during the online install
will remain the same when the install is offline.

## Full offline install

This setup allows you to perform a full offline install as long as you have all the proper dependancies.

The dependancies are the following:

 1. A full Ubuntu mirror for the distribution you will be using in your deployment
 2. A full pypi mirror
 3. A bundle with all the dependancy packages that are hosted on our S3 mirror.

In this documentation, we will assume that you already have your pypi mirror and ubuntu mirror accessible in your
offline network.

### Create a package bundle

From an internet connected station, that can be your development box, generate a bundle of the files that will be
needed by the installer and git bundles of all the repositories.

#### First, Clone the repo

Create your Assemblyline working directory:

    export ASSEMBLYLINE_DIR=~/git/al
    mkdir -p ${ASSEMBLYLINE_DIR}

Then clone the main Assemblyline repo:

    cd $ASSEMBLYLINE_DIR
    git clone https://bitbucket.org/cse-assemblyline/assemblyline.git -b prod_3.2

#### Clone other repos

    ${ASSEMBLYLINE_DIR}/assemblyline/al/run/setup_dev_environment.py

#### Run the create_bundle_script

This step will create a ***package_bundle.tgz*** file in your home directory under an ***al_bundles*** directory
containing all the packages need for offline installation of all non-commercial services as well as a git bundle of
all assemblyline repos that you can use offline to clone the repos.

    cd ~
    ${ASSEMBLYLINE_DIR}/assemblyline/al/run/admin/create_package_bundle.py ~/al_bundles

### Documentation amendments

The following steps guide you for specific parts that need to change from the original online documentations.

#### Make git repos available offline

With all the git bundles that were created in the previous script, make sure the you copy those bundles to a
central offline server or on every single boxes where clones need to be performed and replace the git cloning
functions with the proper method.

#### Make packages available offline

The easiest way to make all packages available to all offline servers would be extract the ***package_bundle.tgz***
files on a central web server offline where all boxes from you installation will have access. Alternatively, you
can copy the ***package_bundle.tgz*** to all servers that will require an install and use a local file transport.

#### Patching the deployment seed

The crutial step to take to make the offline installation work is to patch the file
***/opt/al/pkg/al_private/seed/deployment.py*** in your deployment directory right after the call to
***/opt/al/pkg/assemblyline/deployment/create_deployment.py*** is done.

Inside this file, you can:

 1. Instruct the installer to use another pip mirror by adding the following lines:

        seed['installation']['pip_index_url'] = 'http://<YOUR_OFFLINE_MIRROR>/web/simple'


 2. Point the installer to your dependancy packages repository

        # If using http
        seed['installation']['external_packages'] = {
            'assemblyline': {
                'transport': 'http',
                'args': {
                    'base': '/assemblyline/',
                    'host': '<YOUR_OFFLINE_MIRROR>'
                }
            }
        }

        # or if your copying the files locally on all server
        seed['installation']['external_packages'] = {
            'assemblyline': {
                'transport': 'local',
                'args': {
                    'base': '/opt/al/var/installdeps/assemblyline/'
                }
            }
        }


 3. Change where the repos will be cloned from

        seed['installation']['repositories']['realms']['bitbucket'] = {
            'branch': 'prod_3.2',
            'key': None,  # key used if you use git:// type url
            'password': None,
            'url': 'http://<YOUR_OFFLINE_MIRROR>/git/',
            'user': None
        }

#### Complete the installation

After the deployment.py file is patched, the installer steps can be run normally and the installer will pull the
appropriate configurations from your seed to complete the installation successfully.

#### Combine all into a site specific seed modifier

The proper way to do all of this is to bundle all these changes into one seed that could be reused between multiple
installations. To do so, create a seed modifier that implements the ***update_seed*** method.

Then instead of having to patch the deployment.py seed every single time, you can save that seed modifier as a new repo,
let's call it ***al_site***, then clone it during the original cloning phase and use that seed during the
create deployment phase.

A seed modifier would look something like this:

    def update_seed(seed):
        seed['installation']['pip_index_url'] = 'http://<YOUR_OFFLINE_MIRROR>/web/simple'

        seed['installation']['external_packages'] = {
            'assemblyline': {
                'transport': 'http',
                'args': {
                    'base': '/assemblyline/',
                    'host': '<YOUR_OFFLINE_MIRROR>'
                }
            }
        }

        seed['installation']['repositories']['realms']['bitbucket'] = {
            'branch': 'prod_3.2',
            'key': None,  # key used if you use git:// type url
            'password': None,
            'url': 'http://<YOUR_OFFLINE_MIRROR>/git/',
            'user': None
        }

This file would be saved in the so-called ***al_site*** repo into let's say a ***seeds*** directory under the
name ***my_site.py***.

Then when you follow the documentation for the installation phase, you would modify the following two things:

    # When we usually clone repos also clone your site (for every single boxes)
    ...
    git clone https://<YOUR_OFFLINE_MIRROR>/git/assemblyline.git -b prod_3.2
    git clone https://<YOUR_OFFLINE_MIRROR>/git/al_site.git -b prod_3.2
    ...

    # Finally, when you usually run the create_deployment.py script use
    # the cloned modifier seed at the end of the command
    ...
    /opt/al/pkg/assemblyline/deployment/create_deployment.py al_site.seeds.my_site
    ...
